package com.myclass.dao;

import java.util.List;

import com.myclass.entity.INhap;
import com.myclass.entity.Student;

public class StudentDaoImpl implements StudentDao, INhap {
	public List<Student> listStudent;

	public StudentDaoImpl(List<Student> _listStudent) {
		this.listStudent = _listStudent;
	}

	public void addNewStudent() {
		while (true) {
			Student st = new Student();
			st.nhapThongTin();
			this.listStudent.add(st);
			if (!INhap.wannaContinue("Bạn có muốn tiếp tục nhập thêm sinh viên"))
				break;
		}
	}

	public void editStudent(int id) {
		Student st_edit = this.findById(id);
		if (st_edit != null) {
			System.out.println("Sinh viên có mã Id: " + st_edit.getId());
			System.out.println("Mời nhập tên mới: ");
			st_edit.setFullname(scan.nextLine());
			System.out.println("Cập nhật thành công");
			return;
		}
		System.out.println("Không tìm thấy sinh viên có id là " + id);
	}

	public void deleteStudent(int id) {
		Student st = this.findById(id);
		if (st != null) {
			this.listStudent.remove(st);
			System.out.println("Đã xóa thành công sinh viên có id là " + st.getId());
			return;
		}
		System.out.println("Không tìm thấy sinh viên có id là " + id);
	}

	public Student findById(int id) {
		for (Student st : this.listStudent) {
			if (st.getId() == id)
				return st;
		}
		return null;
	}

}
