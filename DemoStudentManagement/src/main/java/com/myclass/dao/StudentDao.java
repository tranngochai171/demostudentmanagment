package com.myclass.dao;

import com.myclass.entity.Student;

public interface StudentDao {
	public void addNewStudent();

	public void editStudent(int id);

	public void deleteStudent(int id);

	public Student findById(int id);
}
