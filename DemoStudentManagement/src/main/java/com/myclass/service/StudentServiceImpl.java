package com.myclass.service;

import com.myclass.dao.StudentDaoImpl;
import com.myclass.entity.INhap;
import com.myclass.entity.Student;

public class StudentServiceImpl implements StudentService, INhap {
	StudentDaoImpl studentDao;

	public StudentServiceImpl(StudentDaoImpl _studentDao) {
		this.studentDao = _studentDao;
	}

	private int getId() {
		int id = INhap.insertInteger("Mời nhập mã id cần tìm: ");
		return id;
	}

	private int inMenuCho1SinhVien() {
		System.out.println("+=====================================+");
		System.out.println("|       Menu                          |");
		System.out.println("| Chọn 0: để thoát                    |");
		System.out.println("| Chọn 1: để xuất thông tin sinh viên |");
		System.out.println("| Chọn 2: để sửa thông tin sinh viên  |");
		System.out.println("| Chọn 3: để xóa sinh viên            |");
		System.out.println("+====================================+");
		return INhap.insertInteger(" Lựa chọn: ");

	}

	private void luaChonCho1SinhVien() {
		Student st = this.studentDao.findById(this.getId());
		if (st != null) {
			System.out.println("Đã tìm thấy sinh viên. Vui lòng lựa chọn chức năng dành cho sinh viên này");
			int luaChon;
			while (true) {
				luaChon = this.inMenuCho1SinhVien();
				switch (luaChon) {
				case 0:
					return;
				case 1:
					System.out.println("Thông tin sinh viên: ");
					st.xuatThongTin();
					break;
				case 2:
					this.studentDao.editStudent(st.getId());
					break;
				case 3:
					this.studentDao.deleteStudent(st.getId());
					return;
				}
			}
		}
		System.out.println("Không tìm thấy sinh viên");
	}

	private int inMenu() {
		System.out.println("+====================================+");
		System.out.println("|       Menu                         |");
		System.out.println("| Chọn 1: để thêm sinh viên mới      |");
		System.out.println("| Chọn 2: để xuất toàn bộ sinh viên  |");
		System.out.println("| Chọn 3: để sửa thông tin sinh viên |");
		System.out.println("| Chọn 4: để xóa sinh viên           |");
		System.out.println("| Chọn 5: để tìm kiếm 1 sinh viên    |");
		System.out.println("+====================================+");
		return INhap.insertInteger(" Lựa chọn: ");
	}

	private void xuatToanBoSinhVien() {
		if (!this.studentDao.listStudent.isEmpty()) {
			System.out.println("Thông tin toàn sinh viên");
			for (Student st : this.studentDao.listStudent)
				st.xuatThongTin();
			return;
		}
		System.out.println("Danh sách chưa có sinh viên");
	}

	public void xuLy() {
		int luaChon;
		while (true) {
			luaChon = this.inMenu();
			switch (luaChon) {
			case 1:
				this.studentDao.addNewStudent();
				break;
			case 2:
				this.xuatToanBoSinhVien();
				break;
			case 3:
				this.studentDao.editStudent(this.getId());
				break;
			case 4:
				this.studentDao.deleteStudent(this.getId());
				break;
			case 5:
				this.luaChonCho1SinhVien();
				break;
			default:
				System.out.println("Mời lựa chọn từ 1 - 5!");
			}
		}
	}
}
