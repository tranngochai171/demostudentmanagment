package com.myclass.main;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.myclass.config.AppConfig;
import com.myclass.service.StudentServiceImpl;

public class Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		StudentServiceImpl studentService = (StudentServiceImpl) context.getBean("studentServiceImpl");
		studentService.xuLy();
		context.close();
	}
}
