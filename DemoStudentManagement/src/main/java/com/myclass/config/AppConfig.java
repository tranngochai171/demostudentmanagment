package com.myclass.config;

import java.util.LinkedList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.myclass.dao.StudentDaoImpl;
import com.myclass.entity.Student;
import com.myclass.service.StudentService;
import com.myclass.service.StudentServiceImpl;

@Configuration
public class AppConfig {
	@Bean
	public List<Student> getNewStudentList() {
		return new LinkedList<Student>();
	}

	@Bean
	public StudentDaoImpl studentDaoImpl() {
		return new StudentDaoImpl(getNewStudentList());
	}

	@Bean
	public StudentService studentServiceImpl() {
		return new StudentServiceImpl(studentDaoImpl());
	}
}
