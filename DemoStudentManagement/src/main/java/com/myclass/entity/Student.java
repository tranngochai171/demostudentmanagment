package com.myclass.entity;

public class Student implements INhap {
	private int id;
	private String fullname;
	private static int id_auto = 1;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public Student() {
		this.id = Student.id_auto++;
	}

	public Student(String fullname) {
		this.id = Student.id_auto++;
		this.fullname = fullname;
	}

	public void nhapThongTin() {
		System.out.print("Mời nhập tên: ");
		this.fullname = scan.nextLine();
	}

	public void xuatThongTin() {
		System.out.println("Id: " + this.id);
		System.out.println("Name: " + this.fullname);
	}
}
