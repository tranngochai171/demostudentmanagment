package com.myclass.entity;

import java.util.Scanner;

public interface INhap {
	Scanner scan = new Scanner(System.in);

	public static boolean wannaContinue(String message) {
		System.out.println(message);
		int choose;
		while (true) {
			choose = INhap.insertInteger("0: để thoát, 1: tiếp tục");
			switch (choose) {
			case 0:
				return false;
			case 1:
				return true;
			default:
				System.out.println("Mời chọn 0 hoặc 1!");
			}
		}
	}

	public static int insertInteger(String message) {
		while (true) {
			try {
				System.out.println(message);
				int number = Integer.parseInt(scan.nextLine());
				return number;
			} catch (Exception e) {
				System.out.println("Mời nhập số nguyên");
			}
		}
	}
}
